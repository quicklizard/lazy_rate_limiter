package lazy_rate_limiter

import (
	"errors"
	"testing"
	"time"
)

func Test_LimiterTake(t *testing.T) {
	key := "TEST_KEY"
	l := New(Options{
		RedisURL:     "redis://localhost:6379/0",
		SyncInterval: 5 * time.Second,
	})
	l.Start()
	l.AddBucket(key, 1*time.Minute, 2)
	t1, _ := l.Take(key)
	t2, _ := l.Take(key)
	t3, _ := l.Take(key)
	if t1 != true {
		t.Errorf("expected first take to be truthy")
	}
	if t2 != true {
		t.Errorf("expected second take to be truthy")
	}
	if t3 != false {
		t.Errorf("expected third take to be falsy")
	}
	l.Stop()
}

func Test_LimiterTake_Sync(t *testing.T) {
	var e = make(chan bool, 1)
	key := "TEST_KEY"
	opts := Options{
		RedisURL:     "redis://localhost:6379/0",
		SyncInterval: 1 * time.Second,
	}
	l1 := New(opts)
	l2 := New(opts)
	l1.Start()
	l2.Start()
	l1.AddBucket(key, 1*time.Minute, 2)
	l2.AddBucket(key, 1*time.Minute, 2)
	l1.Take(key)
	l1.Take(key)
	l1.Take(key)
	time.AfterFunc(2*time.Second, func() {
		e <- true
	})
	<-e
	t3, _ := l2.Take(key)
	l1.Stop()
	l2.Stop()
	if t3 != false {
		t.Errorf("expected second limited take to be false")
	}
}

func Test_Limiter_OnError(t *testing.T) {
	errChan := make(chan error, 1)
	err := errors.New("test error")
	opts := Options{
		RedisURL:     "redis://localhost:6379/0",
		SyncInterval: 1 * time.Second,
	}
	l1 := New(opts)
	l1.OnError(errChan)
	l1.writeError(err)
	e := <-errChan
	if e.Error() != "test error" {
		t.Errorf("expected limiter error to equal 'test error'")
	}
}

func Test_Limiter_Sync(t *testing.T) {
	var e = make(chan bool, 1)
	key := "TEST_KEY"
	opts := Options{
		RedisURL:     "redis://localhost:6379/0",
		SyncInterval: 1 * time.Second,
	}
	l1 := New(opts)
	l1.Start()
	bkt := l1.AddBucket(key, 1*time.Minute, 2)
	l1.Take(key)
	l1.Take(key)
	l1.Take(key)
	l1.Take(key)
	l1.Take(key)
	if bkt.capacity != 5 {
		t.Errorf("expected bucket capacity to equal 5. got %d instead", bkt.capacity)
	}
	time.AfterFunc(2*time.Second, func() {
		e <- true
	})
	<-e
	l1.Stop()
	if bkt.capacity != 2 {
		t.Errorf("expected bucket capacity to equal 2. got %d instead", bkt.capacity)
	}
}
