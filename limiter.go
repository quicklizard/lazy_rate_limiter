package lazy_rate_limiter

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"net/url"
	"strconv"
	"time"
)

// Limiter manages buckets, and syncs their state to and from Redis.
// It is used as the main entry point to the rate-limiting functionality and encapsulates
// bucket management and bucket-capacity access.
// Buckets are managed in Redis as sorted sets with time-based scores (saved as UnixNano), and set members are expired
// based on their score, in relation to current time. This ensures a sliding-log behaviour where older entries are cleaned up
// based on the limit and time-interval of each bucket.
type Limiter struct {
	buckets    map[string]*Bucket
	errorsChan chan error
	options    Options
	rdb        *redis.Client
	running    bool
	ticker     *time.Ticker
	syncer     chan msg
}

// New creates a new Limiter object with options and returns it
func New(options Options) *Limiter {
	ticker := time.NewTicker(options.SyncInterval)
	l := &Limiter{
		buckets: make(map[string]*Bucket),
		options: options,
		running: false,
		ticker:  ticker,
		syncer:  make(chan msg),
	}
	return l
}

// Start connects to Redis backend, and runs internal goroutine that executes
// async state syncing logic from and to Redis.
// It returns an error if Redis connection fails
func (l *Limiter) Start() error {
	u, err := url.Parse(l.options.RedisURL)
	if err != nil {
		return fmt.Errorf("cannot parse Redis URL - %v", err)
	}
	db, err := strconv.Atoi(u.Path[1:])
	if err != nil {
		return fmt.Errorf("cannot parse Redis DB - %v", err)
	}
	rdb := redis.NewClient(&redis.Options{
		Addr: u.Host,
		DB:   db,
	})
	l.rdb = rdb
	l.running = true
	go l.listenToMessages()
	go l.periodicalSync()
	return nil
}

// Stop tears down the Limiter's execution flow by stopping async state sync goroutine and disconnecting from Redis
func (l *Limiter) Stop() {
	l.running = false
	close(l.syncer)
	l.ticker.Stop()
	l.rdb.Close()
}

// OnError accepts an error channel that can be used to listen to internal errors
// that occur during Redis operations and handle them
func (l *Limiter) OnError(c chan error) {
	l.errorsChan = c
}

// AddBucket creates a new rate-limiting bucket on the provided key, with the given
// time interval and maximum limit. If a bucket already exists for the given key, it is returned.
// Otherwise a new bucket is created and returned.
func (l *Limiter) AddBucket(key string, interval time.Duration, limit int64) *Bucket {
	var b *Bucket
	var found bool
	if b, found = l.buckets[key]; found {
		return b
	}
	b = newBucket(key, interval, limit, l.syncer)
	l.buckets[key] = b
	return b
}

// Take will try to take a capacity of 1 from the bucket identified by key.
// If the bucket is found, Take will return a boolean flag indicating whether capacity if available
// (true) or not (false), along with nil error.
// If the bucket isn't found, Take will return false, along with an error indicating the bucket is missing.
func (l *Limiter) Take(key string) (bool, error) {
	var b *Bucket
	var found bool
	if b, found = l.buckets[key]; !found {
		return false, fmt.Errorf("bucket with key %s is not defined", key)
	}
	return b.take(), nil
}

// TakeOrAdd will look for a bucket identified by key, creating it if it doesn't exist, using
// provided interval and limit, and returning a boolean flag indicating whether capacity is available
// on the bucket (true) or not (false).
func (l *Limiter) TakeOrAdd(key string, interval time.Duration, limit int64) bool {
	var b *Bucket
	var found bool
	if b, found = l.buckets[key]; !found {
		b = l.AddBucket(key, interval, limit)
	}
	return b.take()
}

// listenToMessages waits for messages from buckets on syncer channel
// and syncs bucket state to Redis
func (l *Limiter) listenToMessages() {
SyncLoop:
	for m := range l.syncer {
		l.syncStateToRedis(m)
		if !l.running {
			break SyncLoop
		}
	}
}

// periodicalSync periodically syncs state from Redis to buckets
func (l *Limiter) periodicalSync() {
SyncLoop:
	for {
		select {
		case <-l.ticker.C:
			l.syncStateFromRedis()
		default:
			if !l.running {
				break SyncLoop
			}
		}
	}
}

// syncStateFromRedis iterates over available buckets, reads current capacity from Redis
// for each bucket key, and syncs it to the bucket. In case Redis capacity is higher
// than bucket capacity, the function will remove old entries from Redis and sync updated
// capacity to the bucket. This ensures older bucket entries (calls to "Take") are cleaned up periodically.
func (l *Limiter) syncStateFromRedis() {
	for _, bucket := range l.buckets {
		ctx := context.Background()
		var capacity int64
		var err error
		capacity, err = l.rdb.ZCard(ctx, bucket.Key).Result()
		if err != nil {
			l.writeError(err)
		} else {
			// if Redis bucket is full, cleanup older entries from Redis key
			if capacity >= bucket.Limit {
				now := time.Now().UTC()
				window := now.Add(-1 * bucket.Interval)
				max := fmt.Sprintf("%d", window.UnixNano())
				cmd := l.rdb.ZRemRangeByScore(ctx, bucket.Key, "0", max)
				removed, err := cmd.Result()
				if err != nil {
					l.writeError(err)
				} else {
					capacity -= removed
				}
			}
			bucket.sync(capacity)
		}
	}
}

// syncStateToRedis accepts a message from a bucket, after Take is called,
// and adds it as a new entry to the Redis bucket
func (l *Limiter) syncStateToRedis(syncMsg msg) {
	ctx := context.Background()
	now := time.Now().UTC().UnixNano()
	memberKey := randomString(10)
	pipeline := l.rdb.TxPipeline()
	pipeline.ZRemRangeByScore(ctx, syncMsg.key, "0", fmt.Sprintf("%d", syncMsg.ts))
	pipeline.ZAdd(ctx, syncMsg.key, &redis.Z{Score: float64(now), Member: memberKey})
	pipeline.Expire(ctx, syncMsg.key, syncMsg.ttl)
	_, err := pipeline.Exec(ctx)
	if err != nil {
		l.writeError(err)
	}
}

// writeError takes an error object and writes it to the Limiter's error channel
// if one is available (i.e. OnError was called).
func (l *Limiter) writeError(err error) {
	if l.errorsChan != nil {
		l.errorsChan <- err
	}
}
