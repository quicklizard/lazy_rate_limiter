package lazy_rate_limiter

import "time"

// Options is a struct holding setup information for
// the Limiter object.
// RedisURL is used to set up a connection to a Redis backend.
// SyncInterval is used to periodically sync state from Redis to buckets
type Options struct {
	RedisURL     string
	SyncInterval time.Duration
}
