package lazy_rate_limiter

import "time"

// msg is an internal sync message object used to sync bucket state
// to Redis, after bucket capacity is taken.
// key holds the bucket key
// ttl holds bucket expiry duration in Redis (bucket will expire after ttl duration with no activity)
// ts holds the timestamp before which bucket entries should be expired
type msg struct {
	key string
	ttl time.Duration
	ts  int64
}
