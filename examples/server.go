package main

import (
	lrl "bitbucket.org/quicklizard/lazy_rate_limiter"
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"
)

var rateLimiter *lrl.Limiter

func setup() {
	opts := lrl.Options{
		RedisURL:     "redis://localhost:6379/0",
		SyncInterval: 1 * time.Second,
	}
	rateLimiter = lrl.New(opts)
	rateLimiter.Start()
	rateLimiter.AddBucket("/hello", 1*time.Minute, 10)
}

func teardown() {
	rateLimiter.Stop()
}

func hello(w http.ResponseWriter, req *http.Request) {
	allowed, _ := rateLimiter.Take("/hello")
	if allowed {
		fmt.Fprintf(w, "hello\n")
	} else {
		err := errors.New("max capacity reached")
		http.Error(w, err.Error(), http.StatusTooManyRequests)
	}
}

func serve(ctx context.Context) (err error) {

	mux := http.NewServeMux()
	mux.Handle("/hello", http.HandlerFunc(hello))

	srv := &http.Server{
		Addr:    ":8080",
		Handler: mux,
	}

	go func() {
		if err = srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen:%+s\n", err)
		}
	}()

	log.Printf("server started")

	<-ctx.Done()

	log.Printf("server stopped")

	ctxShutDown, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer func() {
		cancel()
	}()

	if err = srv.Shutdown(ctxShutDown); err != nil {
		log.Fatalf("server Shutdown Failed:%+s", err)
	}

	log.Printf("server exited properly")

	if err == http.ErrServerClosed {
		err = nil
	}

	return
}

func main() {
	log.Printf("server starting")
	setup()
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	ctx, cancel := context.WithCancel(context.Background())

	go func() {
		oscall := <-c
		log.Printf("system call:%+v", oscall)
		cancel()
	}()

	if err := serve(ctx); err != nil {
		log.Printf("failed to serve:+%v\n", err)
	}
	teardown()
	log.Printf("done!")
}
