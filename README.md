# Lazy Rate Limiter

Lazy rate limiter is a simple sliding log rate limiter, with lazy state sync from Redis. It's useful for use-cases where you have
a need to apply a sliding-log time-based rate-limiting logic, that is shared by multiple processes (for example API servers),
and are willing to compromise state accuracy for speed.

## Components

The library exposes two structures - a `Bucket` which encapsulates rate-limiting logic for a distinct key, and a `Limiter` which manages
buckets and syncs their state to and from Redis.

Bucket state is synced to Redis every time available capacity is taken from it. State is synced from Redis to all buckets periodically,
to avoid having to query Redis every time capacity is needed. This may result in a slight out-of-sync state between buckets running in two
separate processes (depending on the Limiter's sync interval), but adds the benefit of not having to perform I/O operations to Redis synchronously.

### Buckets

Rate limiting logic is encapsulated in buckets. Each bucket has a unique key (for example, your API request path), a max limit, and a time
interval. For example, a bucket with the key `/api/v1/notes`, a limit of `10` and an interval of `1 * time.Minute` will let you limit
requests to the above key to 10 requests per minute.

### Limiter

Buckets are managed by a `Limiter` struct, that is responsible for the following:

1. Accessing buckets and asking for capacity
2. Syncing local bucket capacity to Redis
3. Periodically syncing state from Redis to each bucket

## Implementation

Start by installing the module in your code (Go Modules are assumed) - `go get -u bitbucket.org/quicklizard/lazy_rate_limiter`

In your code base add the following:

```go
package main

import (
	lrl "bitbucket.org/quicklizard/lazy_rate_limiter"
	"errors"
	"fmt"
	"net/http"
	"time"
)

var rateLimiter *lrl.Limiter

func setup() {
	opts := lrl.Options{
		RedisURL:     "redis://localhost:6379/0",
		SyncInterval: 1 * time.Second,
	}
	rateLimiter = lrl.New(opts)
	rateLimiter.Start()
	rateLimiter.AddBucket("/hello", 1*time.Minute, 10)
}

func hello(w http.ResponseWriter, req *http.Request) {
	allowed, _ := rateLimiter.Take("/hello")
	if allowed {
		fmt.Fprintf(w, "hello\n")
	} else {
		err := errors.New("max capacity reached")
		http.Error(w, err.Error(), http.StatusTooManyRequests)
	}
}

func main() {
    setup()
	http.HandleFunc("/hello", hello)
	http.ListenAndServe(":8080", nil)
    // rateLimiter.Stop() should be called before your code exits
}
```

For a complete example, see `examples/server.go`

## Credits

I used the basic Redis sorted-set logic from [Implementing a sliding log rate limiter with Redis and Golang](https://levelup.gitconnected.com/implementing-a-sliding-log-rate-limiter-with-redis-and-golang-79db8a297b9e)
as the inspiration for this library.

## License

This code is available under MIT License.