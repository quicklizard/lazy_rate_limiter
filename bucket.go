package lazy_rate_limiter

import (
	"sync"
	"time"
)

// Bucket holds rate-limiting capabilities for a given unique key, with defined limit in duration.
// For example, a bucket for the key "TEST" with a limit of 10 in a time interval of 1 minute will allow
// 10 operations (Takes) in 1 minute.
type Bucket struct {
	Interval time.Duration
	Limit    int64
	Key      string

	capacity int64
	lock     sync.Mutex
	syncer   chan msg
}

// newBucket creates a new Bucket, with a given key, time interval and limit and returns it.
// syncer channel is used to send messages from the bucket, whenever capacity is accessed, to
// the managing Limiter object, and onwards to Redis.
func newBucket(key string, interval time.Duration, limit int64, syncer chan msg) *Bucket {
	return &Bucket{
		Interval: interval,
		Limit:    limit,
		Key:      key,
		capacity: 0,
		lock:     sync.Mutex{},
		syncer:   syncer,
	}
}

// take tries to take available capacity from the bucket. If capacity is available
// a sync message is sent to Limiter, and from it to Redis, and true is returned.
// false is returned otherwise.
func (b *Bucket) take() bool {
	b.lock.Lock()
	defer b.lock.Unlock()
	if b.capacity >= b.Limit {
		return false
	}
	b.capacity += 1
	now := time.Now().UTC()
	window := now.Add(-1 * b.Interval)
	syncMsg := msg{key: b.Key, ts: window.UnixNano(), ttl: b.Interval}
	b.syncer <- syncMsg
	return b.capacity <= b.Limit
}

// sync accepts a capacity parameter and updates internal capacity counter. it is used
// to sync bucket state from Redis by the managing Limiter object.
func (b *Bucket) sync(cap int64) {
	b.lock.Lock()
	defer b.lock.Unlock()
	if cap <= b.Limit {
		b.capacity = cap
	} else {
		b.capacity = b.Limit
	}
}
